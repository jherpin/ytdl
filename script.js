/**
 * Dependencies
 */
var youtubedl = require('youtube-dl');
var Q = require('q');
var clc = require('cli-color');
var ffmpeg = require('ffmpeg-node');
var fs = require('fs');
var rimraf = require('rimraf');
var qs = require('qs');
var youtube = require('./youtube');
var config = require('./config');

/**
 * Constants
 */
var TMP_FOLDER = './tmp';
var IMG_FOLDER = './img';
var RESULT_FILE = TMP_FOLDER + '/out.mp4';

/**
 * CLI colors methods
 */
var error = clc.red.bold;
var warn = clc.yellow;

/**
 * Functions
 */
var clearTmp = function() {
  var deferred = Q.defer();
  rimraf(TMP_FOLDER, function() {
    fs.mkdir(TMP_FOLDER, function() {
      return deferred.resolve(config.source);
    });
  });
  return deferred.promise;
};

/**
 * Get info for a Youtube video
 */
var getInfo = function(url) {
  
  var deferred = Q.defer();

  youtubedl.getInfo(url, [], function(err, info) {
    if(err) deferred.reject(err);
  	else deferred.resolve({ url: url, info: info});
  });

  return deferred.promise;

};

/**
 * Download a Youtube video
 */
var download = function(args) {

  var deferred = Q.defer();
  var dl = youtubedl.download(args.url, TMP_FOLDER, ['--max-quality=18', '--output=tmp.mp4']);

  // Download starts
  dl.on('download', function(data) {
    console.log('Download started (' + data.filename + ' - ' + data.size + ')');
  });

  dl.on('progress', function(data) {
  	process.stdout.write(data.eta + ' ' + data.percent + '% at ' + data.speed + '\r');
  });

  // On download error
  dl.on('error', function(err) {
    deferred.reject(err);
  });

  // On download end successfully
  dl.on('end', function(data) {
  	console.log('Download ended');
    deferred.resolve({ filename: data.filename, info: args.info });
  });

  return deferred.promise;
};

/**
 * Get sound from video and merge with an image
 */
var extractSoundFromVideo = function(args) {

  var deferred = Q.defer();
  var filename = TMP_FOLDER + '/' + args.filename; 

  console.log('Start converting video file to mp3');
  ffmpeg.exec(['-i', filename, filename + '.mp3'], function() {
    console.log('Video file converted to mp3');
    deferred.resolve({ filename: filename + '.mp3', info: args.info });
  });

  return deferred.promise;

};

/**
 * Merge audio with image
 * ffmpeg -i tmp/lol-90AiXO1pAiA.mp4.mp3 -loop 1 -i img/test.png -shortest tmp/res.mp4
 */
var mergeAudioWithImage = function(args) {

  var deferred = Q.defer();

  console.log('Start merging picture with sound');
  ffmpeg.exec(['-i', args.filename, '-loop', 1, '-i',  IMG_FOLDER + '/' + config.image, '-shortest', '-strict', 'experimental', RESULT_FILE], function() {
    console.log('New video file created from image and audio file');
    deferred.resolve(args.info);
  });

  return deferred.promise;

};

/**
 * Upload the video to Youtube
 */
var upload = function(info) {

  var deferred = Q.defer();

  console.log('Start uploading the video');

  var video = youtube
    .createUpload(RESULT_FILE)
    .user(config.user)
    .source('YTUploader')
    .password(config.password)
    .key(config.key)
    .title(config.title)
    .description(config.description)
    .category(config.category)
    .keywords(config.keywords)
    .upload(function(err, res){
      if (err) deferred.reject(err);
      else deferred.resolve();
    });

  return deferred.promise;

};

/**
 * Main
 */
clearTmp()
  .then(getInfo)
  .then(download)
  .then(extractSoundFromVideo)
  .then(mergeAudioWithImage)
  .then(upload)
  .catch(function(err) {
    console.log(error(err));
  })
  .done(function() {
    console.log('This is the end');
    process.exit(1);
  });
